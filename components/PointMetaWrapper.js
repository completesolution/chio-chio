import React from 'react'
import { getStore } from '../store/registrator'
import { getPointAddress } from '../store/features/data'
import { getCurrentPoint } from '../store/features/current-point'

export default class PointMetaWrapper extends React.Component {
	static navigationOptions({ navigation }) {
		const { getState } = getStore()
		const state = getState()

		return {
			title: getPointAddress(state, getCurrentPoint(state))
		}
	}

	render() {
		return (
			<React.Fragment>
				{this.props.children}
			</React.Fragment>
		)
	}
}
