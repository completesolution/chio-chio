import React from 'react'
import { Marker } from 'react-native-maps'
import { Image } from 'react-native'
import { connect } from 'react-redux'
import {
	getPointLatitude,
	getPointLongitude,
	getCityLatitude,
	getCityLongitude
} from '../store/features/data'
import { showSelectedCity } from '../store/features/current-point'

import { NavigationActions } from 'react-navigation'
const { navigate, back } = NavigationActions

const MapMarker = ({ latitude, longitude, onPress, pointKey }) => (
	<Marker
		coordinate={{
			latitude,
			longitude
		}}
		onPress={() => onPress(pointKey)}
	>
		<Image
			style={{
				width: 35,
				height: 35
			}}
			source={require('../assets/pin.png')}
		/>
	</Marker>
)

export default connect(
	(state, { pointKey }) => {
		let latitude = getPointLatitude(state, pointKey)
		let longitude = getPointLongitude(state, pointKey)

		if (!latitude) {
			// console.log('latitude 1', latitude);
			latitude = getCityLatitude(state, pointKey)
			// console.log('latitude 2', latitude);
			longitude = getCityLongitude(state, pointKey)
		}

		return {
			latitude,
			longitude
		}
	},
	dispatch => ({
		onPress: (pointKey) => {
			dispatch(showSelectedCity(pointKey))
		}
	})
)(MapMarker)
