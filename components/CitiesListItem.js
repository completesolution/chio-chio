import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Touchable from 'react-native-platform-touchable'

const CitiesListItem = ({ city, onSelect }) => (
	<Touchable
		background={Touchable.Ripple('#ccc', false)}
		style={styles.option}
		onPress={onSelect.bind(null, city)}
	>
		<View style={styles.optionTextContainer}>
			<Text style={styles.optionText}>
				{city}
			</Text>
		</View>
	</Touchable>
)

const styles = StyleSheet.create({
   optionsTitleText: {
      fontSize: 16,
      marginLeft: 15,
      marginTop: 9,
      marginBottom: 12,
		fontFamily: 'proxima-nova-regular'
   },
   option: {
      backgroundColor: '#fdfdfd',
      paddingHorizontal: 15,
      paddingVertical: 15,
      borderBottomWidth: StyleSheet.hairlineWidth,
      borderBottomColor: '#EDEDED'
   },
   optionText: {
      fontSize: 15,
      marginTop: 1,
		fontFamily: 'proxima-nova-regular'
   }
})

export default CitiesListItem
