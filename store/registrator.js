let _STORE_ = {}

export const registerStore = store => _STORE_ = store
export const getStore = () => _STORE_
