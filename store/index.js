import coreReducer, {
	loadDataFromStorage,
	loadDataFromGoogle,
	saveDataToStorage,
	isDateTimePassed,
	setUpdateFrequency
} from './features/core'
import { getGoogleInitialData } from './features/core/requests'
import dataReducer, { isDataEmpty } from './features/data'
import mapReducer from './features/map'
import errorReducer, { isError } from './features/error'
import currentPointReducer from './features/current-point'
import thunk from 'redux-thunk'
import { NavigationActions } from 'react-navigation'
import { registerStore } from './registrator'

import {
	createStore,
	applyMiddleware,
	compose
} from 'redux'

import {
  combineReducers
} from 'redux-immutable'

import { connect } from 'react-redux'

import {
	createReduxContainer,
	createReactNavigationReduxMiddleware,
	createNavigationReducer
} from 'react-navigation-redux-helpers'

import AppNavigator from '../navigation/AppNavigator'

const delay = delay => new Promise(resolve => setTimeout(resolve, delay))

const launchTheApp = async ({ dispatch, getState }) => {
	const { navigate } = NavigationActions
	// console.log('Loading from storage')
	await dispatch(loadDataFromStorage())
	// console.log('Loaded from storage', getState())
	let navigated = false

	// console.log('Loading from VPS')
	await dispatch(loadDataFromGoogle())
	// console.log('Loaded from VPS')

	await delay(1500)
	if (!isDataEmpty(getState())) {
		dispatch(navigate({ routeName: 'CitiesList' }))
		navigated = true
	} else while (isDataEmpty(getState())) {
		// console.log('Data empty')
		await delay(500)
	}

	// console.log('Save to storage')
	dispatch(saveDataToStorage())

	// console.log('Navigate')
	if (!navigated) {
		dispatch(navigate({ routeName: 'CitiesList' }))
	}
}

export const { store, App } = (() => {
	const reducer = combineReducers({
		nav: createNavigationReducer(AppNavigator),
		core: coreReducer,
		data: dataReducer,
		map: mapReducer,
		error: errorReducer,
		currentPoint: currentPointReducer
	})

	const navigationMiddleware = createReactNavigationReduxMiddleware(state => state.nav)

	const App = connect(state => ({
		state: state.get('nav')
	}))(createReduxContainer(AppNavigator))

	const store = createStore(
		reducer,
		applyMiddleware(
			thunk,
			navigationMiddleware
		)
	)

	registerStore(store)

	launchTheApp(store)

	return { store, App }
})()
