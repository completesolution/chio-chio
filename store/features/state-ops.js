import { mapKeys, mapValues, isFunction, replace, lowerFirst } from 'lodash'

export default paths => {
	paths = mapKeys(paths, (value, key) =>
		lowerFirst(replace(key, 'to', '')))

	return {
		get: mapValues(paths, explorer => (state, ...args) =>
			state.getIn(explorer(...args))),

		set: mapValues(paths, explorer => (state, ...args) => {
			const value = args.pop()

			if (isFunction(value)) {
				return state.updateIn(explorer(...args), value)
			}

			return state.setIn(explorer(...args), value)
		}),

		remove: mapValues(paths, explorer => (state, ...args) =>
			state.removeIn(explorer(...args))),

		merge: mapValues(paths, explorer => (state, ...args) => {
			const value = args.pop()

			return state.mergeIn(explorer(...args), value)
		}),

		map: mapValues(paths, explorer => (state, ...args) => {
			const value = args.pop()

			return state.updateIn(explorer(...args), state => state.map(value))
		})
	}
}
