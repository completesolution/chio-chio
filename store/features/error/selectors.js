import { from } from './paths'
import getOps from '../state-ops'

const { get } = getOps(from('error'))

export const getError = state => get.error(state)
export const isError = state => get.error(state) !== ''
