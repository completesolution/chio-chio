import { createActions } from 'redux-actions'
import { identity } from 'lodash'

export const { setError } = createActions({
	SET_ERROR: identity
})
