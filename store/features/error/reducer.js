import { handleActions } from 'redux-actions'
import { setError } from './actions'

export default handleActions({
	[setError]: (state, { payload: message }) => message
}, '')
