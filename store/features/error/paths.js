import createPaths from '../paths'

export const { path, from } = createPaths({
	toError: path =>
		[...path.toRoot()]
})
