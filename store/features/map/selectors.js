import { from } from './paths'
import getOps from '../state-ops'
import { getPoints, getCities } from '../data'
import { List } from 'immutable'
import moize from 'moize'

const { get } = getOps(from('map'))

export const getLatitude = state => get.latitude(state)
export const getLongitude = state => get.longitude(state)
export const getLatitudeDelta = state => get.latitudeDelta(state)
export const getLongitudeDelta = state => get.longitudeDelta(state)

const pathToLongitude = ['meta', 'geo', 'longitude']
const pathToLatitude = ['meta', 'geo', 'latitude']

const memoizedPointGetter = moize((
  _longitude,
  _latitude,
  _longitudeDelta,
  _latitudeDelta,
  longitude,
  latitude,
  longitudeDelta,
  latitudeDelta,
  state
) => {
  const points = getPoints(state)
  let list = []

  if (longitudeDelta < 1.7) {
    const points = getPoints(state)
    list = List(points
      .filter(point => {
        const pointLatitude = point.getIn(pathToLatitude)
        const pointLongitude = point.getIn(pathToLongitude)

        return pointLatitude >= latitude - longitudeDelta && pointLongitude >= longitude - longitudeDelta &&
          pointLatitude <= latitude + latitudeDelta && pointLongitude <= longitude + longitudeDelta
      })
      .keys())
  } else {
    const points = getCities(state)

    list = List(points
      .filter(point => {
        const pointLatitude = point.get('latitude')
        const pointLongitude = point.get('longitude')

        return pointLatitude >= latitude - longitudeDelta && pointLongitude >= longitude - longitudeDelta &&
          pointLatitude <= latitude + latitudeDelta && pointLongitude <= longitude + longitudeDelta
      })
      .keys())
  }


  return list
}, {
  maxAge: 1000 * 60 * 5,
  maxArgs: 4
})

const decimals = 10

const roundRegion = ({
	longitude,
	latitude,
	longitudeDelta,
	latitudeDelta
}) => ([
	Math.round(longitude * decimals) / decimals,
	Math.round(latitude * decimals) / decimals,
	Math.round(longitudeDelta * decimals) / decimals,
	Math.round(latitudeDelta * decimals) / decimals,
	longitude,
	latitude,
	longitudeDelta,
	latitudeDelta
])

export const getLimitedPoints = state => region => {
  return memoizedPointGetter(...roundRegion(region), state)
}
