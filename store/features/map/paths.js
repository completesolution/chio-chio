import createPaths from '../paths'

export const { path, from } = createPaths({
	toMap: path =>
		[...path.toRoot()],
	toLatitude: path =>
		[...path.toMap(), 'latitude'],
	toLongitude: path =>
		[...path.toMap(), 'longitude'],
	toLatitudeDelta: path =>
		[...path.toMap(), 'latitudeDelta'],
	toLongitudeDelta: path =>
		[...path.toMap(), 'longitudeDelta'],
})
