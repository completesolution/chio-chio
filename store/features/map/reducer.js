import { handleActions } from 'redux-actions'
import { path } from './paths'
import getOps from '../state-ops'
import { setRegion } from './actions'
import { Map } from 'immutable'

const { set } = getOps(path)

export default handleActions({
	[setRegion]: (state, { payload }) => set.map(state, payload)
}, Map({
	latitude: 55.7733945,
	longitude: 49.1408859,
	latitudeDelta: 0.3,
	longitudeDelta: 0.4
}))
// }, Map({
// 	latitude: 55.603085 + 0.17,
// 	longitude: 48.823508 + 0.3,
// 	latitudeDelta: 0.3,
// 	longitudeDelta: 0.4
// }))
