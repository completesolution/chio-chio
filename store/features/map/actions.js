import { createActions } from 'redux-actions'
import { identity } from 'lodash'
import {
	getLatitude,
	getLongitude,
	getLatitudeDelta,
	getLongitudeDelta
} from './selectors'
import {
	getCityRegion
} from '../data'

import { NavigationActions } from 'react-navigation'
const { navigate } = NavigationActions

export const { setRegion } = createActions({
	SET_REGION: identity
})

export const showCity = city => (dispatch, getState) => {
	const state = getState()
	dispatch(setRegion(getCityRegion(state, city)))
	dispatch(navigate({ routeName: 'Map' }))
}
