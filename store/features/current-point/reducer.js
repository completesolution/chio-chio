import { handleActions } from 'redux-actions'
import { setCurrentPoint } from './actions'
import { Map, fromJS } from 'immutable'

export default handleActions({
	[setCurrentPoint]: (state, { payload: currentPoint }) => {
		return currentPoint
	}
}, '')
