import createPaths from '../paths'

export const { path, from } = createPaths({
	toCurrentPoint: path =>
		[...path.toRoot()]
})
