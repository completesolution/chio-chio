import { from } from './paths'
import getOps from '../state-ops'

const { get } = getOps(from('currentPoint'))

export const getCurrentPoint = state => get.currentPoint(state)
