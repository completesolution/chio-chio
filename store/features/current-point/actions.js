import { createActions } from 'redux-actions'
import { identity } from 'lodash'

import { NavigationActions } from 'react-navigation'
const { navigate } = NavigationActions

export const { setCurrentPoint } = createActions({
	SET_CURRENT_POINT: identity
})

export const showSelectedCity = pointKey => (dispatch, getState) => {
	dispatch(setCurrentPoint(pointKey))
	dispatch(navigate({ routeName: 'PointNavigator' }))
}
