import { stringify } from 'query-string'
import moment from 'moment'
import matrix from 'matrix-js'
// import { needLazyReload }
import {
	flow,
	map,
	chunk,
	split,
	join,
	trim,
	capitalize,
	reduce,
	lowerCase,
	filter,
	mapValues,
	update,
	toNumber,
	get,
	result,
	findIndex,
	isArray,
	isEmpty,
	isNaN,
	isUndefined,
	isNumber,
	pickBy
} from 'lodash/fp'

import {
	NeedReloadError,
	EmptyPointsError
} from './errors'

const delay = delay => new Promise(resolve => setTimeout(resolve, delay))
const resToJSON = res => res.json()
const filterNotCity = filter(title => title !== 'Города')
const log = label => arg => {
	return arg
}
const checkFor = (ErrorClass, cb) => data => {
	const isNeedToThrow = !cb(data)

	if (isNeedToThrow) {
		throw new ErrorClass()
	}

	return data
}
const not = f => arg => !f(arg)
const or = (...fs) => arg =>
	fs
		.map(f => f(arg))
		.reduce((result, value) => result || value, false)
const and = (...fs) => arg =>
	fs
		.map(f => f(arg))
		.reduce((result, value) => result && value, true)

const prepareCities = flow(
	get('values'),
	reduce((cities, [name, lowerCorner, upperCorner], index) => {

		if (
			isUndefined(name) ||
			isUndefined(lowerCorner) ||
			isUndefined(upperCorner)
		) {
			if (prepareCities.firstCall) {
				throw new NeedReloadError()
			}
			return cities
		}

		lowerCorner = lowerCorner.split(' ')
		upperCorner = upperCorner.split(' ')
		let longitude = toNumber(lowerCorner[0])
		let latitude = toNumber(lowerCorner[1])
		const longitudeDelta = toNumber(upperCorner[0]) - longitude
		const latitudeDelta = toNumber(upperCorner[1]) - latitude


		if (isNaN(longitude) || isNaN(latitude) ||
			isNaN(longitudeDelta) || isNaN(latitudeDelta)) {
			if (prepareCities.firstCall) {
				throw new NeedReloadError()
			}
			return cities
		}

		prepareCities.firstCall = false

		longitude += longitudeDelta * 0.5
		latitude += latitudeDelta * 0.5

		return ({
			...cities,
			[name]: {
				longitude,
				latitude,
				longitudeDelta,
				latitudeDelta
			}
		})
	}, {})
)

prepareCities.firstCall = true

const formatGoogleSheetDescriptorUrl = (key, sheetId) =>
	`https://sheets.googleapis.com/v4/spreadsheets/${
		sheetId
	}?${
		stringify({ key })
	}`

const prepareSheetTitles = flow(
	get('sheets'),
	checkFor(NeedReloadError, isArray),
	map(get('properties.title')),
	filter(title => title !== ''),
	checkFor(NeedReloadError, data => !isEmpty(data)),
)

// const prepareSheetTitles = ({ sheets }) =>
// 	sheets.map(descriptor => descriptor.properties.title)

const getGoogleSheetTitles = (key, sheetId) =>
	fetch(formatGoogleSheetDescriptorUrl(key, sheetId))
		.then(resToJSON)
		.then(prepareSheetTitles)
		.catch(() => throw new NeedReloadError())

const pointTitleToRange = title => [
	`'${title}'!A1:E`
]

const prepareRanges = ranges => ranges.reduce((result, range, index) => ({
	...result,
	[`ranges${index}`]: range
}), {})

const prepareQuery = sheetTitles => {
	return prepareRanges([
		`'Города'!A1:C`,
		...filterNotCity(sheetTitles)
			.map(pointTitleToRange)
			.reduce((ranges, subRanges) => [...ranges, ...subRanges], [])
	])
}

const formatGoogleSheetDataUrl = (key, sheetId) => query => {
	return `https://sheets.googleapis.com/v4/spreadsheets/${
		sheetId
	}/values:batchGet?${
		stringify({
	   	...query,
	   	key
		})
	}`.replace(/ranges\d+/g, 'ranges')
}


const preparePointMeta = flow(
	([[city], [address], [geo], , [description]]) => ({
		city, address, geo, description
	}),
	update('city', lowerCase),
	update('geo', flow(
		geo => geo || 'a a',
		split(' '),
		map(toNumber),
		([longitude, latitude]) => ({
			longitude,
			latitude
		})
	)),
	update('description', trim)
)

const prepareDate = flow(
	trim,
	split(/[,\. \/\\]/),
	([day, month, year]) => [year, month, day],
	join('-'),
	moment,
	result('valueOf')
)

const prepareMasters = flow(
	split(/[,\.\/\\]/),
	map(trim),
	filter(master => master !== ''),
	map(split(' ')),
	map(flow(
		filter(word => word !== ''),
		map(capitalize),
		join(' ')
	))
)

const preparePointSchedule = flow(
	map(([date, masters]) => ({
		date: prepareDate(date),
		masters: prepareMasters(masters)
	})),
	filter(flow(
		get('date'),
		not(isNaN)
	))
)

const preparePoints = flow(
	map('values'),
	map(matrix),
	map(matrix => [
		matrix([0, 4], 1), //meta,
		matrix([], [0, 1]).slice(8).filter(([elem]) => elem !== ''), //services,
		matrix([], [3, 4]).slice(7).filter(([elem]) => elem !== ''), //shchedule
	]),
	map(([rawMeta, services, rawSchedule]) => ({
		meta: preparePointMeta(rawMeta),
		services,
		schedule: preparePointSchedule(rawSchedule)
	})),
	filter(flow(
		get('meta.geo.latitude'),
		and(
			isNumber,
			not(isNaN),
			not(isUndefined)
		)
	)),
	reduce((points, point) => ({
		...points,
		[`${point.meta.city}:${point.meta.address}`]: point
	}), {})
)

const findCityRangeIndex = findIndex(
	flow(
		get('range'),
		range => /Города/.test(range)
	)
)

const shiftCities = valueRanges => {
	const index = findCityRangeIndex(valueRanges)
	const cityRange = valueRanges[index]
	valueRanges.splice(index, 1)
	return cityRange
}

const prepareSheetData = ({ valueRanges }) => ({
	cities: prepareCities(shiftCities(valueRanges)),
	points: preparePoints(valueRanges)
})

export const getGoogleInitialData = () =>
	fetch('http://open.chio-chio.ru/json/data.json')
		.then(resToJSON)
		.then(({ uri }) => uri)

export const getGoogleSheetData = async (uri) => {
	const data = await fetch(uri)

	return data
}
