import createPaths from '../paths'

export const { path, from } = createPaths({
	toCore: path =>
		[...path.toRoot()],
	toLoding: path =>
		[...path.toCore(), 'loading'],
	toLastUpdateDate: path =>
		[...path.toCore(), 'lastUpdateDate'],
	toDateTime: path =>
		[...path.toCore(), 'dateTime'],
	toUpdateFrequency: path =>
		[...path.toCore(), 'updateFrequency']
})
