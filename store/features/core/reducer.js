import { handleActions } from 'redux-actions'
import { path } from './paths'
import getOps from '../state-ops'
import { updateDateTime, setUpdateFrequency, setDateTime } from './actions'
import { Map } from 'immutable'

const { set } = getOps(path)

export default handleActions({
	[updateDateTime]: state => set.dateTime(state, Date.now()),
	[setDateTime]: (state, { payload: dateTime }) => set.dateTime(state, dateTime),
	[setUpdateFrequency]: (state, { payload: updateFrequency }) => {
		return set.updateFrequency(state, updateFrequency * 1000)
	}
}, Map({
	dateTime: 0,
	updateFrequency: 0
}))
