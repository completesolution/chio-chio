import { assign } from 'lodash'

const generateErrors = names => names
	.reduce((collection, name) => ({
		...collection,
		[name]: class extends Error {
			constructor(parentError) {
				super(name)

				if (parentError) {
					assign(this, parentError)
				}
			}
		}
	}), {})

export const {
	NeedReloadError,
	EmptyPointsError
} = generateErrors([
	'NeedReloadError',
	'EmptyPointsError'
])
