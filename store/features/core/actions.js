import { createActions } from 'redux-actions'
import { identity } from 'lodash'
import { getUser, getGoogleKey, isDateTimePassed, getDateTime } from './selectors'
import { AsyncStorage } from 'react-native'
import { setData, getData, isDataEmpty } from '../data'
import { getGoogleInitialData, getGoogleSheetData } from './requests'
import { NeedReloadError } from './errors'
import { setError } from '../error'
import { isEmpty } from 'lodash/fp'

const delay = duration => new Promise(resolve => setTimeout(resolve, duration))

export const {
	startLoading,
	stopLoading,
	setUpdateFrequency,
	updateDateTime,
	setDateTime
} = createActions({
	START_LOADING: identity,
	STOP_LOADING: identity,
	SET_UPDATE_FREQUENCY: identity,
	UPDATE_DATE_TIME: identity,
	SET_DATE_TIME: identity
})

export const loadDataFromStorage = () => async (dispatch, getState) => {
	await AsyncStorage
		.getItem('chio-data-3')
		.then((data = '') => JSON.parse(data))
		.then(data => {
			dispatch(setData(data))
		})

	// await AsyncStorage
	// 	.getItem('chio-date-time')
	// 	.then(time => Number(time || 0))
	// 	.then(time => {
	// 		dispatch(setDateTime(time))
	// 	})
}

export const loadDataFromGoogle = () => async (dispatch, getState) => {
	let K = 0

	const updateData = async () => {
		try {
			const uri = await getGoogleInitialData()
			const { _bodyInit: data } = await getGoogleSheetData(uri)
			if (isEmpty(data) || data === '{}') {
				throw new Error()
			}
			dispatch(setData(JSON.parse(data)))
			setTimeout(updateData, 30000)
		} catch(err) {
			K += 1
			if (K === 3) {
				dispatch(setError('К сожалению, произошла ошибка...\nПопробуйте запустить приложение позже'))
			}
			setTimeout(updateData, 1000)
		}

	}

	updateData()
}

export const saveDataToStorage = () => async (dispatch, getState) => {
	await AsyncStorage
		.setItem('chio-data-3', JSON.stringify(getData(getState()).toJS()))
	// await AsyncStorage
	// 	.setItem('chio-date-time', getDateTime(getState()).toString())
}
