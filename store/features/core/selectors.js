import { from } from './paths'
import getOps from '../state-ops'
import { isUndefined } from 'lodash'

const { get } = getOps(from('core'))
const DAY = 0// 1 * 24 * 60 * 60 * 1000

export const getLoading = state => get.loading(state)
export const getLastUpdateDate = state => get.lastUpdateDate(state)
export const getUpdateFrequency = state => get.updateFrequency(state)
export const getDateTime = state => get.dateTime(state)
export const isDateTimePassed = state => {
	const dateTime = getDateTime(state)
	return Date.now() - dateTime >= getUpdateFrequency(state)
}
