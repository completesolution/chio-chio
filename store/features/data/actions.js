import { createActions } from 'redux-actions'
import { identity } from 'lodash'

export const { setData } = createActions({
	SET_DATA: identity
})
