import createPaths from '../paths'

export const { path, from } = createPaths({
	toData: path =>
		[...path.toRoot()],
	toCities: path =>
		[...path.toData(), 'cities'],
	toCity: (path, key) =>
		[...path.toData(), 'cities', key],
	toCityLongitude: (path, key) =>
		[...path.toCity(key), 'longitude'],
	toCityLatitude: (path, key) =>
		[...path.toCity(key), 'latitude'],
	toRegion: (path, city) =>
		[...path.toCities(), city],
	toPoints: path =>
		[...path.toData(), 'points'],
	toPoint: (path, key) =>
		[...path.toPoints(), key],
	toPointMeta: (path, key) =>
		[...path.toPoint(key), 'meta'],
	toPointGeo: (path, key) =>
		[...path.toPointMeta(key), 'geo'],
	toPointLatitude: (path, key) =>
		[...path.toPointGeo(key), 'latitude'],
	toPointLongitude: (path, key) =>
		[...path.toPointGeo(key), 'longitude'],
	toPointDescription: (path, key) =>
		[...path.toPointMeta(key), 'description'],
	toPointCity: (path, key) =>
		[...path.toPointMeta(key), 'city'],
	toPointAddress: (path, key) =>
		[...path.toPointMeta(key), 'address'],
	toPointSchedule: (path, key) =>
		[...path.toPoint(key), 'pointSchedule'],
	toPointScheduleDay: (path, key, day) =>
		[...path.toPointSchedule(key), day],
	toSchedule: (path, key) =>
		[...path.toPoint(key), 'schedule'],
	toServices: (path, key) =>
		[...path.toPoint(key), 'services']
})
