import { handleActions } from 'redux-actions'
import { setData } from './actions'
import { Map, fromJS } from 'immutable'

export default handleActions({
	[setData]: (state, { payload: data }) => fromJS(data)
}, fromJS({
	points: {},
	cities: {}
}))
