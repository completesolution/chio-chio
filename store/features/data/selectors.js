import { from } from './paths'
import getOps from '../state-ops'
import { isUndefined, isEmpty } from 'lodash'
import { List, Map } from 'immutable'

const { get } = getOps(from('data'))
const DAY = 24 * 60 * 60 * 1000

const megaCities = [
	'Москва',
	'Санкт-Петербург',
	'Новосибирск',
	'Екатеринбург',
	'Нижний Новгород',
	'Казань',
	'Челябинск',
	'Омск',
	'Самара',
	'Ростов-на-Дону',
	'Уфа',
	'Красноярск',
	'Пермь',
	'Воронеж',
	'Волгоград'
]

export const getData = state => get.data(state)
// export const isDataEmpty = state => isEmpty(getData(state))
export const getCities = state => get.cities(state) || Map()
export const getCitiesCount = state => List(getCities(state).keys()).count() || 145
export const getCitiesList = state => {
	const buff = []

	return List(getCities(state).keys())
		.sort((a, b) => {
			if (a < b) return -1
			if (a > b) return 1
			if (a === b) return -1
		})
		.filter(name => {
			if (megaCities.includes(name)) {
				buff.push(name)
				return false
			}

			return true
		})
		.unshift(...buff)
}

export const getCityRegion = (state, name) => get.region(state, name)

export const getCityLatitude = (state, key) => get.cityLatitude(state, key)
export const getCityLongitude = (state, key) => get.cityLongitude(state, key)
export const getPoints = state => get.points(state) || Map()
export const getPointsCount = state => List(getPoints(state).keys()).count() || 194
export const getPointsList = state => List(getPoints(state).keys())
export const getPointLatitude = (state, key) => get.pointLatitude(state, key)
export const getPointLongitude = (state, key) => get.pointLongitude(state, key)
export const getPointCity = (state, key) => get.pointCity(state, key)
export const getPointAddress = (state, key) => get.pointAddress(state, key)
export const getPointDescription = (state, key) => get.pointDescription(state, key)

export const isDataEmpty = state =>
	isEmpty(getData(state)) ||
	isEmpty(getCities(state)) ||
	getCitiesCount(state) === 0

export const getTodayMasters = (state, key) => {
	let schedule = get
		.schedule(state, key)
		.find(schedule => {
			const delta = Date.now() - schedule.get('date')
			return delta > 0 && delta <= DAY
		})

	if (!isUndefined(schedule)) {
		return schedule.get('masters').toJS()
	}
}

export const getTomorrowMasters = (state, key) => {
	let schedule = get
		.schedule(state, key)
		.find(schedule => {
			const delta = schedule.get('date') - Date.now()
			return delta > 0 && delta <= DAY
		})

	if (!isUndefined(schedule)) {
		return schedule.get('masters').toJS()
	}
}

export const getPointSchedule = (state, key) => {
	if (!get.pointSchedule(state, key)) return false

	const result = []

	for (let i = 0; i < 7; i += 1) {
		const day = get.pointScheduleDay(state, key, i)
		if (day.get('startTime') === '-') {
			result.push('Выходной')
		} else {
			result.push(`${day.get('startTime')} — ${day.get('endTime')}`)
		}
	}

	return result
}

export const getDayAfterTomorrowMasters = (state, key) => {
	let schedule = get
		.schedule(state, key)
		.find(schedule => {
			const delta = schedule.get('date') - Date.now()
			return delta > DAY && delta <= (DAY * 2)
		})

	if (!isUndefined(schedule)) {
		return schedule.get('masters').toJS()
	}
}

export const getServices = (state, key) => get.services(state, key)
