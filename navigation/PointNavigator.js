import React from 'react'
import { createBottomTabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'

import PointDescription from '../screens/PointDescription'
import PointSchedule from '../screens/PointSchedule'
import PointServices from '../screens/PointServices'
import { getStore } from '../store/registrator'
import { getCurrentPoint } from '../store/features/current-point'
import { getPointCity, getPointAddress } from '../store/features/data'
import { capitalize, flow, map, join, split, trim } from 'lodash/fp'

const capiS = flow(
	split(' '),
	map(capitalize),
	join(' '),
	trim
)

const capiD = flow(
	split('-'),
	map(capitalize),
	join('-'),
	trim
)

import { connect } from 'react-redux'

const BottomTobNavigator = createBottomTabNavigator({
	PointDescription,
	PointSchedule,
	PointServices
}, {
	defaultNavigationOptions: ({ navigation }) => ({
		tabBarIcon: ({ focused, horizontal, tintColor }) => {
			const { routeName } = navigation.state
			let iconName

			switch (routeName) {
				case 'PointDescription':
					iconName = 'ios-information-circle-outline'
					break
				case 'PointSchedule':
					iconName = 'ios-calendar'
					break
				case 'PointServices':
					iconName = 'ios-list'
					break
			}

			return <Ionicons name={iconName} size={25} color={tintColor} />
		}
	}),
	tabBarOptions: {
		activeTintColor: '#FF0000',
		inactiveTintColor: '#BCBEC0',
		labelStyle: {
			fontFamily: 'proxima-nova-regular'
		}
	}
})

BottomTobNavigator.navigationOptions = () => {
	const state = getStore().getState()
	const pointKey = getCurrentPoint(state)
	const city = getPointCity(state, pointKey)
	const address = getPointAddress(state, pointKey)

	let capi

	if (city.indexOf('-') === -1) {
		capi = capiS(city)
	} else {
		capi = capiD(city)
	}

	return {
		title: `${capi} | ${address}`
	}
}

export default BottomTobNavigator
