import { createStackNavigator } from 'react-navigation'
import CitiesList from '../screens/CitiesList'
import Map from '../screens/Map'
import Loading from '../screens/Loading'
import PointDescription from '../screens/PointDescription'
import PointNavigator from './PointNavigator'
import { Platform } from 'react-native'

export default createStackNavigator({
	CitiesList: {
		screen: CitiesList,
		navigationOptions: {
			headerLeft: null,
			headerTitleStyle: {
				fontFamily: 'proxima-nova-bold',
				fontWeight: 'normal'
			}
		}
	},
	Map: {
		screen: Map,
		navigationOptions: {
			headerTitleStyle: {
				fontFamily: 'proxima-nova-bold',
				fontWeight: 'normal'
			}
		}
	},
	Loading: {
		screen: Loading,
		navigationOptions: {
			header: null
		}
	},
	PointNavigator: {
		screen: PointNavigator,
		navigationOptions: {
			headerTitleStyle: {
				fontFamily: 'proxima-nova-bold',
				fontWeight: 'normal'
			}
		}
		// screen: PointDescription
	}
}, {
	initialRouteName: 'Loading'
})
