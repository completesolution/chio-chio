import React from 'react'
import {StyleSheet, Text, ScrollView, View, Image, SectionList} from 'react-native'
import { connect } from 'react-redux'
import PointMetaWrapper from '../components/PointMetaWrapper'
import { getServices } from '../store/features/data'
import { getCurrentPoint } from '../store/features/current-point'
import { List, ListItem } from 'react-native-elements'

class PointServices extends React.Component {
	static navigationOptions = {
      title: 'Услуги'
   }

	render() {
		const { services } = this.props

		return (
			<PointMetaWrapper>
				<ScrollView style={styles.container}>
					{services.map(([label, price], index) => <ListItem
						title={
							<Text style={{
								fontFamily: 'proxima-nova-regular'
							}}>
								{label}
							</Text>
						}
						key={index}
						bottomDivider
						badge={{
							value: `${price}₽`,
							textStyle: {
								color: 'white',
								fontSize: 14,
								fontFamily: 'proxima-nova-regular'
							},
							badgeStyle: {
								margin: -5,
								backgroundColor: '#FF0000'
							}
						}}
					/>)}
				</ScrollView>
			</PointMetaWrapper>
		)
	}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff'
   }
})

export default connect(
	state => ({
		services: getServices(state, getCurrentPoint(state))
	})
)(PointServices)
