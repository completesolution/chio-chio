import React from 'react'
import {StyleSheet, Text, ScrollView, View, Image, Linking} from 'react-native'
import MapView, {Marker} from 'react-native-maps'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux'
import {
	getLatitude,
	getLongitude,
	getLatitudeDelta,
	getLongitudeDelta
} from '../store/features/map'
import { getPointsList } from '../store/features/data'
import MapMarker from '../components/MapMarker'
import PointMetaWrapper from '../components/PointMetaWrapper'
import { getPointDescription, getPointSchedule } from '../store/features/data'
import { getCurrentPoint } from '../store/features/current-point'

const Schedule = ({ data, day }) => (
	<View style={styles.list}>
		<Text style={{fontFamily: day === 1 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[0] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Пн. </Text>
			{data[0]}
		</Text>
		<Text style={{fontFamily: day === 2 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[1] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Вт. </Text>
			{data[1]}
		</Text>
		<Text style={{fontFamily: day === 3 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[2] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Ср. </Text>
			{data[2]}
		</Text>
		<Text style={{fontFamily: day === 4 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[3] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Чт. </Text>
			{data[3]}
		</Text>
		<Text style={{fontFamily: day === 5 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[4] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Пт. </Text>
			{data[4]}
		</Text>
		<Text style={{fontFamily: day === 6 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[5] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Сб. </Text>
			{data[5]}
		</Text>
		<Text style={{fontFamily: day === 0 ? 'proxima-nova-bold' : 'proxima-nova-regular'}}>
			<Ionicons name={data[6] === 'Выходной' ? 'ios-cafe' : 'ios-time'} color={'rgb(255, 0, 0)'} />
			<Text style={styles.dayLabel}>  Вс. </Text>
			{data[6]}
		</Text>
	</View>
)

class PointDescription extends React.Component {
	static navigationOptions = {
      title: 'Описание'
   }

	callPhone() {
		Linking.openURL('tel://+78002001327').catch((err) => { /* ... */ })
	}

	render() {
		const { region, points, description, schedule, day } = this.props

		return (
			<PointMetaWrapper>
				<View style={styles.container}>
					<Text
						style={{
							fontFamily: 'proxima-nova-bold',
							color: 'rgb(255, 0, 0)',
							marginBottom: 25,
							textAlign: 'center'
						}}
						onPress={() => this.callPhone()}
					>
				   	<Text style={{ fontSize: 16 }}>{'+7 (800) 200-13-27\n'}</Text>
				   	<Text style={{ fontSize: 14 }}>{'Бесплатный звонок по РФ'}</Text>
					</Text>
			   	<Text style={{fontFamily: 'proxima-nova-regular'}}>{description}</Text>
					{schedule && <Schedule data={schedule} day={(new Date).getDay()} />}
				</View>
			</PointMetaWrapper>
		)
	}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center',
		alignItems: 'center',
		padding: 20
   },
	list: {
		paddingTop: 20,
		justifyContent: 'flex-start',
	},
	dayLabel: {
		fontFamily: 'proxima-nova-bold',
		color: 'rgb(255, 0, 0)'
	},
	daySelected: {
		fontFamily: 'proxima-nova-bold'
	}
})

export default connect(
	state => {
		const currentPoint = getCurrentPoint(state)

		return {
			description: getPointDescription(state, currentPoint),
			schedule: getPointSchedule(state, currentPoint)
		}
	}
)(PointDescription)
