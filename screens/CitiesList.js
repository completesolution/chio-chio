import React from 'react'
import {StyleSheet, ScrollView, View} from 'react-native'
import Touchable from 'react-native-platform-touchable'
import CitiesListItem from '../components/CitiesListItem'
import { connect } from 'react-redux'
import { getCitiesList } from '../store/features/data'
import { showCity } from '../store/features/map'

const defaultList = ['Казань', 'Москва', 'Уфа']

class CitiesList extends React.Component {
	static navigationOptions = {
      title: 'Города'
   }

	render() {
		const { cities = defaultList, onCitySelect } = this.props
		
		return (
			<ScrollView>
		   	{cities.map((city, index) => (
					<CitiesListItem
						city={city}
						key={index}
						onSelect={onCitySelect}
					/>
				))}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingTop: 15
   }
})

export default connect(
	state => {
		return {
			cities: getCitiesList(state)
		}
	},
	dispatch => ({
		onCitySelect: city => {
			dispatch(showCity(city))
		}
	})
)(CitiesList)
