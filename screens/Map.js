import React from 'react'
import {StyleSheet, Text, ScrollView, View, Image} from 'react-native'
import MapView, {Marker} from 'react-native-maps'
import { connect } from 'react-redux'
import {
	getLatitude,
	getLongitude,
	getLatitudeDelta,
	getLongitudeDelta
} from '../store/features/map'
import { getPointsList } from '../store/features/data'
import { getLimitedPoints } from '../store/features/map'
import MapMarker from '../components/MapMarker'

let dragTimeout = 0

class Map extends React.Component {
	constructor(props) {
		super(props)
		const { region } = props

		this.state = {
			points: [],
			region
		}
	}

	static navigationOptions = {
      title: 'Карта'
   }

	updateRegion = (region) => {
		this.setState({ region })
	}

	updatePoints = (region, delay = 500) => {
		clearTimeout(dragTimeout)
		dragTimeout = setTimeout(() => {
			this.setState({
				points: this.props.getPoints(region)
			})
		}, delay)
	}

	componentDidMount() {
		this.updatePoints(this.state.region, 0)
	}

	componentDidUpdate(prevProps, prevState) {
		const { region } = this.state
		if (prevState.region !== region) {
			this.updatePoints(region)
		}
	}

	render() {
		const { updateRegion, props, state } = this
		const { region } = props
		const { points } = state

		return (
			<View style={styles.container}>
		   	<MapView
					style={{
                  flex: 1
               }}
					initialRegion={region}
					onRegionChange={updateRegion}
				>
					{points.map(key => (
						<MapMarker pointKey={key} key={key} />
					))}
				</MapView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
      justifyContent: 'center'
   }
})

export default connect(
	state => ({
		region: {
			latitude: getLatitude(state),
			longitude: getLongitude(state),
			latitudeDelta: getLatitudeDelta(state),
			longitudeDelta: getLongitudeDelta(state)
		},
		points: getPointsList(state),
		getPoints: getLimitedPoints(state)
	})
)(Map)
