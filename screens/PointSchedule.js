import React from 'react'
import {StyleSheet, Text, ScrollView, View, Image, SectionList} from 'react-native'
import { connect } from 'react-redux'
import PointMetaWrapper from '../components/PointMetaWrapper'
import {
	getTodayMasters,
	getTomorrowMasters,
	getDayAfterTomorrowMasters
} from '../store/features/data'
import { getCurrentPoint } from '../store/features/current-point'
import { List, ListItem } from 'react-native-elements'

class PointSchedule extends React.Component {
	static navigationOptions = {
      title: 'Мастера'
   }

	render() {
		const { sections } = this.props

		return (
			<PointMetaWrapper>
				<ScrollView style={styles.container}>
					<SectionList
						renderItem={({item, index}) => (
							<ListItem
								key={index}
								title={
									<Text style={{
										fontFamily: 'proxima-nova-regular'
									}}>
										{item}
									</Text>
								}
								bottomDivider
								fontFamily={'proxima-nova-regular'}
							/>
						)}
						renderSectionHeader={({section: {title}}) => (
							<ListItem
								title={
									<View style={{
										backgroundColor: '#FF0000',
										margin: -15,
										padding: 15
									}}>
										<Text style={{
											fontWeight: 'normal',
											fontSize: 20,
											color: 'white',
											fontFamily: 'proxima-nova-bold'
										}}>
											{title}
										</Text>
									</View>
								}
								bottomDivider
							/>
						)}
						sections={sections}
						keyExtractor={(item, index) => item + index}
					/>
					{/* {today && today.map(
						(master, index) => (
							<ListItem
								key={index}
								title={master}
								bottomDivider
							/>
						)
					)} */}
				</ScrollView>
			</PointMetaWrapper>
		)
	}
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff'
   }
})

export default connect(
	state => {
		const today = getTodayMasters(state, getCurrentPoint(state))
		const tomorrow = getTomorrowMasters(state, getCurrentPoint(state))
		const dayAfterTomorrow = getDayAfterTomorrowMasters(
			state,
			getCurrentPoint(state)
		)

		const sections = []

		if (today) sections.push({
			title: 'Сегодня',
			data: today
		})

		if (tomorrow) sections.push({
			title: 'Завтра',
			data: tomorrow
		})

		if (dayAfterTomorrow) sections.push({
			title: 'Послезавтра',
			data: dayAfterTomorrow
		})

		if (!sections.length) sections.push({
			title: 'Расписание отсутсвует',
			data: []
		})

		return ({ sections })
	}
)(PointSchedule)
