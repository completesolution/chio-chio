import React from 'react'
import {StyleSheet, Text, ScrollView, View, ImageBackground, Image, Dimensions} from 'react-native'
import MapView, {Marker} from 'react-native-maps'
import { connect } from 'react-redux'
import {
	getLatitude,
	getLongitude,
	getLatitudeDelta,
	getLongitudeDelta
} from '../store/features/map'
import { getPointsCount, getCitiesCount } from '../store/features/data'
import MapMarker from '../components/MapMarker'
import { isError } from '../store/features/error'

const deviceWidth = Dimensions.get('window').width

class Loading extends React.Component {
	static navigationOptions = {
		title: 'Загрузка'
	}

	render() {
		const { isError, citiesCount, pointsCount } = this.props

		let citiesLabel
		let pointsLabel

		switch (citiesCount % 10) {
			case 0:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
				citiesLabel = 'Городов'
				break
			case 1:
				citiesLabel = 'Город'
				break
			case 2:
			case 3:
			case 4:
				citiesLabel = 'Города'
				break
			default:
				citiesLabel = 'Городов'
				break
		}

		if (citiesCount % 100 < 20 && citiesCount % 100 >= 10) {
			citiesLabel = 'Городов'
		}

		if (pointsCount % 10 === 1) {
			pointsLabel = 'Парикмахерская'
		} else {
			pointsLabel = 'Парикмахерских'
		}

		if (pointsCount % 100 === 11) {
			pointsLabel = 'Парикмахерских'
		}

		return (
			<View style={styles.container}>
				<ImageBackground
					source={require('../assets/splash.png')}
					style={styles.background}
					resizeMode={'cover'}
				>
					<View style={styles.header}>
						<View style={styles.logoWrapper}>
 							<Image
								style={styles.logo}
 								source={require('../assets/logo.png')}
								resizeMode={'contain'}
 							/>
						</View>
						<View style={styles.labelWrapper}>
							<Text style={styles.label}>
								<Text style={styles.labelText}>
									{'СЕТЬ ЯПОНСКИХ\nПАРИКМАХЕРСКИХ\n'}
								</Text>
								<Text style={styles.labelNumber}>
									{'№1'}
								</Text>
							</Text>
						</View>
						<View style={styles.indicatorsWrapper}>
							<View style={styles.indicator}>
								<Text style={styles.indicatorText}>
									<Text style={styles.indicatorCount}>{citiesCount}</Text>
									<Text style={styles.indicatorLabel}>{`\n${citiesLabel}`}</Text>
								</Text>
							</View>
							<View style={styles.indicator}>
								<Text style={styles.indicatorText}>
									<Text style={styles.indicatorCount}>{pointsCount}</Text>
									<Text style={styles.indicatorLabel}>{`\n${pointsLabel}`}</Text>
								</Text>
							</View>
						</View>
					</View>
					<View style={styles.footer}>
						<Text>{
 							isError ?
 								'Ошибка, пожалуйста, зайдите в приложение позже' :
 								'Загрузка...'
 						}</Text>
					</View>
				</ImageBackground>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		justifyContent: 'center',
		alignItems: 'center'
	},
	header: {
		flex: 5
	},
	logoWrapper: {
		flex: 2,
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: '10%',
		marginLeft: '30%',
		marginRight: '30%'
	},
	logo: {
		width: deviceWidth * 0.4,
		height: undefined,
		aspectRatio: 1
		// height: '100%'
	},
	labelWrapper: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	label: {
		fontSize: 30,
		textAlign: 'center'
	},
	labelText: {
		fontFamily: 'proxima-nova-bold'
	},
	labelNumber: {
		fontFamily: 'proxima-nova-thin'
	},
	indicatorsWrapper: {
		flex: 1,
		flexDirection: 'row',
		marginLeft: '12%',
		marginRight: '12%'
	},
	indicator: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		width: '50%'
	},
	indicatorText: {
		textAlign: 'center'
	},
	indicatorCount: {
		fontFamily: 'proxima-nova-bold',
		fontSize: 29
	},
	indicatorLabel: {
		fontFamily: 'proxima-nova-regular',
		fontSize: 12
	},
	footer: {
		flex: 2,
		justifyContent: 'flex-end',
		alignItems: 'center',
		paddingBottom: 35
	},
	background: {
		width: '100%',
		height: '100%'
	}
})

export default connect(
	state => ({
		isError: isError(state),
		pointsCount: getPointsCount(state),
		citiesCount: getCitiesCount(state)
	})
)(Loading)
