import React from 'react'
import { Font } from 'expo'
import {StyleSheet, Text, View, StatusBar} from 'react-native'
import { store, App } from './store'
import { Provider } from 'react-redux'

const mountComponent = () => (<App />)

export default class Root extends React.Component {
	state = {
	   fontLoaded: false
	}

	async componentWillMount() {
	   await Font.loadAsync({
			'proxima-nova-regular': require('./assets/fonts/Proxima-Nova-Regular.otf'),
			'proxima-nova-bold': require('./assets/fonts/Proxima-Nova-Bold.otf'),
			'proxima-nova-thin': require('./assets/fonts/Proxima-Nova-Thin.otf')
		})

		this.setState({ fontLoaded: true })
	}

   render() {
		const { fontLoaded } = this.state

		let App = null

		if (fontLoaded){
			App = mountComponent()
		}

      return (
				<Provider store={store}>
					<>
						<StatusBar barStyle={'dark-content'} />
						{App}
					</>
      	</Provider>
			)
   }
}
